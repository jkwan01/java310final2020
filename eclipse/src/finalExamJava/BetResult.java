package finalExamJava;

import java.util.*;

//BetResult class that handles the backend, processes the dice roll and keeps track of the money
public class BetResult {
	private String choice;
	private int money = 500;
	private int result;
	private int bet;
	private Random rand = new Random();
	
	//set the choice of the user
	public void setChoice(String choice) {
		this.choice = choice;
	}
	
	//set the bet
	public void setBet(int bet) {
		this.bet = bet;
	}
	
	//roll the dice to get the number and get the result
	public boolean rollDice() {
		result = rand.nextInt(6);
		if ((choice.equals("small") && result == 1) || (choice.equals("small") && result == 2) || (choice.equals("small") && result == 3) || (choice.equals("large") && result == 4) || (choice.equals("large") && result == 5) || (choice.equals("large") && result == 6)) {
			money = money + bet;
			return true;
		}
		else {
			if (money >=0) {
				money = money - bet;
			}
			if (money < 0) {
				money = 0;
			}
			return false;
		}
	}
	
	//some messages the user can receive depending on the result of the roll
	public String result() {
		if (rollDice()) {
			return "Win!";
		}
		else {
			return "Loss!";
		}
	}
	
	//returns the amount of money the user currently has
	public int getMoney() {
		return money;
	}
}
