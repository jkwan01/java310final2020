package finalExamJava;

import java.util.Arrays;

public class Recursion {
	public static void main(String[] args) {
		String[] test = {"word", "notword", "popw", "anotherword", "fillthearrw"};
		System.out.println(recursiveCount(test,1));
	}
	
	public static int recursiveCount(String[] words, int n) {
		return recursiveCount(words, n, n, words.length -1, 0);
	}
	public static int recursiveCount(String[] words, int n, int start, int end, int count) {
		if (words.length < n) {
			return 0;
		}
		if (start == end) {
			return count;
		}
		if (start%2 == 1 && words[start].contains("w")) {
			return recursiveCount(words, n, start+1, end, count+1);
		}
		else {
			return recursiveCount(words, n, start+1, end, count);
		}
	}
}
