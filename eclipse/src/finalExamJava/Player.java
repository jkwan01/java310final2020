//Player class where some error cases are handled, and the general displaying to the user
package finalExamJava;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.text.*;

public class Player implements EventHandler<ActionEvent>{
	private TextField bet;
	private BetResult result;
	private Text currentAmount;
	private Text winOrLoss;
	
	//constructor
	public Player(TextField bet, Text currentAmount, Text winOrLoss, BetResult result, String choice, int betAmount) {
		this.bet = bet;
		this.currentAmount = currentAmount;
		this.winOrLoss = winOrLoss;
		this.result = result;
		this.result.setChoice(choice);
	}
	
	//handle override 
	public void handle(ActionEvent e) {
		try {
			if (Integer.parseInt(bet.getText()) <= result.getMoney()) {
				this.result.setBet(Integer.parseInt(bet.getText()));
				winOrLoss.setText(result.result());
				currentAmount.setText(result.getMoney() + "");
			}
			else {
				winOrLoss.setText("Bet is bigger than current money.");
			}
		}
		catch(Exception err) {
			winOrLoss.setText("An error has occurred. Please try again.");
		}

	}
}
